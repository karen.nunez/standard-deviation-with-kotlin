//COMPILAR : kotlinc hello.kt -include-runtime -d hello.jar
//EJECUTAR : java -jar hello.jar

/***
 - Al ponerle "!!" después de readLine() le indicamos que lea el valor que ingresamos, sea cual sea
 - La librería java.util.Scanner nos permite utilizar entradas por parte del usuario
 - Con split(",") separamos los datos ingresados por el usuario para poder manejarlos
 - La estructura de datos Map utiliza una clave para acceder a un valor, en este caso nos sirve para acceder al valor
       de nuestro arreglo y lo convierte a "Int" para poder manejarlo después
*/

import java.util.Scanner
import java.util.*

fun main() {
    with(Scanner(System.`in`)) {
        while (true){
            println("Insert integer numeric values separated by commas: ")
            var datos = readLine()!!
            if (datos == "exit"){
                println("You left the program")
                System.exit(0)
            }
            else if (datos.equals("") || datos.equals(" ")){
                println("Please insert integer numeric values separated by commas")
            } 
            else {
                try {
                    var numeros = datos.split(",").map { it.toInt() }
                    var n = numeros.count()
                    var media = numeros.average()
                    var sumatoria = 0.0
                    
                    
                    for (i in numeros) {
                        var x = Math.pow(i - media,2.0)
                        sumatoria += x
                    }
                    var desviacion = Math.sqrt(sumatoria/n)
                
                    println("The standard deviation is: %.4f".format(desviacion))
                } catch (e: NumberFormatException){
                    println("Some value is wrong. Please insert integer numeric values separated by commas.")
                }
            } 
        }
    }

}